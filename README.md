# Solutis - Robocode

Robocode aims to program a battle tank robot that is able to compete against others in an arena.

<!--ts-->
 * [About My Robot](#about)
 * [Requirements](#requirements)
 * [Follow Me](#follow)
<!--te-->

<br></br>
<br></br>
<br></br>

# 🤖 About My Robot
The "Bahea" robot was developed with the objective of being a fast and aggressive robot. At the same time that he manages to dodge the opponent's shots, it contains high precision and a high firing speed making it almost impossible for the opponents to survive. I feel sorry for those who will face my "Bahea".

<br></br>
<br></br>

# ⚡ Requirements and Technologies

- [x] Eclipse
- [x] Java
- [x] Robocode.jar 
- [x] Robocode.bat

<br></br>
<br></br>

# 😜 Follow Me
<!--ts-->
 * [Instagram](https://www.instagram.com/tiko.lo/)
 * [GitHub](https://github.com/carlosdevv)
<!--te-->

