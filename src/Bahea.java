package NewRobot;

import robocode.*;
import static robocode.util.Utils.normalRelativeAngleDegrees;
import java.awt.Color;




public class Bahea extends AdvancedRobot
{
	boolean movingForward; 
	boolean inWall;
	int direction = 1;
	
	public void run() {

		setColors();

	
		setAdjustRadarForRobotTurn(true);
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);
		turnRadarRightRadians(Double.POSITIVE_INFINITY);
		
		if (getX() <= 50 || getY() <= 50
				|| getBattleFieldWidth() - getX() <= 50
				|| getBattleFieldHeight() - getY() <= 50) {
			this.inWall = true;
		} else {
			this.inWall = false;
		}

		setAhead(40000);
		setTurnRadarRight(360);
		this.movingForward = true;

		while (true) {
		
			if (getX() > 50 && getY() > 50
					&& getBattleFieldWidth() - getX() > 50
					&& getBattleFieldHeight() - getY() > 50
					&& this.inWall == true) {
				this.inWall = false;
			}
			if (getX() <= 50 || getY() <= 50
					|| getBattleFieldWidth() - getX() <= 50
					|| getBattleFieldHeight() - getY() <= 50) {
				if (this.inWall == false) {
					reverseDirection();
					inWall = true;
				}
			}

	
			if (getRadarTurnRemaining() == 0.0) {
				setTurnRadarRight(360);
			}

			execute();
		}
	}

	public void onHitWall(HitWallEvent e) {
		reverseDirection();
	}

public void onScannedRobot(ScannedRobotEvent e) {

	    double absBearing=e.getBearingRadians()+getHeadingRadians();
		double latVel=e.getVelocity() * Math.sin(e.getHeadingRadians() -absBearing);
		double gunTurnAmt;

		setTurnRadarLeftRadians(getRadarTurnRemainingRadians());

		if(Math.random()>.9){
			setMaxVelocity((12*Math.random())+12);
		}
        
		if (e.getDistance() > 150) {
			gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing- getGunHeadingRadians()+latVel/22);
			setTurnGunRightRadians(gunTurnAmt);
			setTurnRightRadians(robocode.util.Utils.normalRelativeAngle(absBearing-getHeadingRadians()+latVel/getVelocity()));
			setAhead((e.getDistance() - 140)*direction);
			setFire(3);
		}
		
		else{
			gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing- getGunHeadingRadians()+latVel/15);
			setTurnGunRightRadians(gunTurnAmt);
			setTurnLeft(-90-e.getBearing()); 
			setAhead((e.getDistance() - 140)*direction);
			setFire(3);
		}	
	}



	public void onHitRobot(HitRobotEvent e) {
		if (e.isMyFault()) {
			reverseDirection();
		}
	}

	private void setColors() {
		setBodyColor(Color.BLUE);
		setGunColor(Color.RED);
		setRadarColor(Color.WHITE);
		setBulletColor(Color.ORANGE);
		setScanColor(Color.GRAY);
	}

	public void reverseDirection() {
		if (this.movingForward) {
			setBack(40000);
			this.movingForward = false;
		} else {
			setAhead(40000);
			this.movingForward = true;
		}
	}

}
